#Python functions to notify when an intrustion is detected. This code is only ran during the testing phase not the training phase

def invalid_pgn(pgn, row):
    print("PGN " + str(pgn) + " invalid for packet")
    print(row)

def invalid_sa(sa, row):
    print("Source Address " + sa + " invalid for packet")
    print(row)

def invalid_dlc(dlc, row):
    print("Data Length Code " + str(dlc) + " invalid for packet")
    print(row)

def invalid_priority(priority, row):
    print("Priority " + str(priority) + " invalid for packet")
    print(row)

def invalid_reserved(reserved, row):
    print("Reserved " + str(reserved) + " invalid for packet")
    print(row)

def invalid_datapage(datapage, row):
    print("DataPage " + str(datapage) + " invalid for packet")
    print(row)

def invalid_pduformat(pduformat, row):
    print("PduFormat " + str(pduformat) + " invalid for packet")
    print(row)

def invalid_pduspecific(pduspecific, row):
    print("PduSpecific " + str(pduspecific) + " invalid for packet")
    print(row)

def invalid_extrabits(extrabits, row):
    print("ExtraBits " + str(extrabits) + " invalid for packet")
    print(row)

def invalid_stuffingbits(row):
    print("StuffingBits are not zero for packet")
    print(row)

def invalid_frequency(frequency, row):
    print("Frequency " + str(frequency) + " invalid for packet")
    print(row)
    
def invalid_pos_rate_of_change_delta(spn_name, rate_of_change, row):
    print("Positive rate of change delta" + str(rate_of_change) + " invalid for " + spn_name)
    print(row)

def invalid_neg_rate_of_change_delta(spn_name, rate_of_change, row):
    print("Negative rate of change delta" + str(rate_of_change) + " invalid for " + spn_name)
    print(row)