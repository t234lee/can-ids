from flask import Flask, request, jsonify
import random
import time
import threading

import settings

app = Flask(__name__)

# List to hold the temperature data points
temperature_data = []

# Sample data structure for PGN frequencies, each entry is a dictionary of (SA, PGN, Frequency)
pgn_frequencies = []

def add_pgn_frequency(pgn, sa, frequency):
    for entry in pgn_frequencies:
        if entry["PGN"] == pgn and entry["SA"] == sa:
            entry["Frequency"] = frequency
            break
    else:
        pgn_frequencies.append({"PGN": pgn, "SA": sa, "Frequency": frequency})

def update_data():
    while True:
        time.sleep(10)  # Wait for 10 seconds

        can_dict = settings.past_values_dict

        # Update temperature
        fluctuation = random.uniform(-0.5, 0.5)
        new_temperature = can_dict.get("130316", {}).get("52", {}).get("LastSPNValues", {}).get("ActualTemperature", None)
        temperature_data.append([new_temperature, int(time.time() * 1000)])
        if len(temperature_data) > 1000:
            temperature_data.pop(0)

        # Update PGN frequencies
        for pgn in can_dict:
            for sa in can_dict[pgn]:
                add_pgn_frequency(pgn, sa, can_dict[pgn][sa].get("LastFrequency", None))

# Start the data update function in a separate thread
threading.Thread(target=update_data, daemon=True).start()

@app.route('/search', methods=['POST'])
def search():
    # Return available targets
    return jsonify(["temperature", "PGN Frequencies"])

@app.route('/query', methods=['POST'])
def query():
    req = request.get_json()
    response_data = []

    targets = [t.get('target') for t in req.get('targets', [])]

    if "temperature" in targets:
        response_data.append({
            "columns": [{"text": "Time", "type": "time"}, {"text": "Temperature", "type": "number"}],
            "rows": [[timestamp, temp] for temp, timestamp in temperature_data],
            "type": "table"
        })

    if "PGN Frequencies" in targets:
        rows = [[pgn_entry["PGN"], pgn_entry["SA"], pgn_entry["Frequency"]] for pgn_entry in pgn_frequencies]
        response_data.append({
            "columns": [
                {"text": "PGN", "type": "string"},
                {"text": "SA", "type": "string"},
                {"text": "Frequency", "type": "number"}
            ],
            "rows": rows,
            "type": "table"
        })

    return jsonify(response_data)

@app.route('/annotations', methods=['POST'])
def annotations():
    return jsonify([])

def run_app():
    app.run(debug=True, port=5000)
