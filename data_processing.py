#Python file to process J1939 .csv files during both training and testing phases
import os
import json
import math

import settings
import notifier
import model_update

from dotenv import load_dotenv

load_dotenv()

mode = os.getenv('MODE')
model_file_path = os.getenv('MODEL_FILEPATH')
ns_frequency_range = int(os.getenv('NS_FREQUENCY_RANGE'))
rate_of_change_error_threshold = float(os.getenv('ROC_ERROR_THRESHOLD'))

def get_model(model_file_path):
    # Opening JSON file
    with open(model_file_path, 'r') as pgn_file:
        # Reading from json file
        valid_pgns = json.load(pgn_file)
    return dict(valid_pgns)

def init_past_values_dict(pgn, sa):
    sa_dict = settings.past_values_dict.get(pgn, {}).get(sa)
    #Initializes a past values dict for each PGN/SA. Only done the first time a message is received from a pgn/sa
    if sa_dict == None:
        past_sa_values_dict = settings.past_values_dict.get(pgn)
        if past_sa_values_dict == None:
            past_sa_values_dict = {}
        past_sa_values_dict[sa] = {}
        settings.past_values_dict[pgn] = past_sa_values_dict
    
#Sets the value for the last timestamp of a pgn/sa
def set_last_timestamp(pgn, sa, timestamp):
    settings.past_values_dict.get(pgn, {}).get(sa)['LastTimestamp'] = timestamp

#Sets the value for the last frequency of a pgn/sa
def set_last_frequency(pgn, sa, frequency):
    settings.past_values_dict.get(pgn, {}).get(sa)['LastFrequency'] = frequency

#Sets the value for the last spn values of a pgn/sa
def set_last_spn_values(pgn, sa, spnvalues):
    curr_spn_value_dict = {}
    for spnvalue in spnvalues:
        curr_spn_value_dict[spnvalue['SPNName']] = spnvalue['DecodedSPNValue']
    settings.past_values_dict.get(pgn, {}).get(sa)['LastSPNValues'] = curr_spn_value_dict

#Function to round the frequency
def round_frequency(frequency):
    #Quick fix for issue with NTP sync giving negative frequencies. We use the absolute value instead to make it always postitive
    #and deal with frequencies of zero
    roundedfrequency = 0
    if frequency != 0:
        nodigits = int(math.log10(abs(frequency)))
        roundedfrequency = round(frequency, -nodigits)
    return roundedfrequency

#Gets the frequency of a message
def get_frequency(pgn, sa, timestamp):
    last_timestamp = settings.past_values_dict.get(pgn, {}).get(sa).get('LastTimestamp')
    if last_timestamp != None:
        frequency = int(timestamp) - int(last_timestamp)
        rounded_frequency = round_frequency(frequency)
        set_last_frequency(pgn, sa, rounded_frequency)
        return rounded_frequency
    
#Check the frequency of a message
def check_frequency(frequency, frequency_list):
    fq_lower_bound = frequency - ns_frequency_range
    fq_upper_bound = frequency + ns_frequency_range
    for i in frequency_list:
        if(i > fq_lower_bound and i < fq_upper_bound):
            return True
    return False

#Checks the pos rate of change of spn values
def check_pos_rate_of_change(rate_of_change, spnrules_dict):
    pos_rate_of_change = spnrules_dict['PositiveRateOfChangeDelta']
    if rate_of_change > (pos_rate_of_change * rate_of_change_error_threshold):
        return False
    return True

#Checks the pos rate of change of spn values
def check_neg_rate_of_change(rate_of_change, spnrules_dict):
    neg_rate_of_change = spnrules_dict['NegativeRateOfChangeDelta']
    if rate_of_change < (neg_rate_of_change * rate_of_change_error_threshold):
        return False
    return True

def test_model(row):
    pgn = str(row['pgn'])
    sa = str(row['sa'])
    dlc = row['dlc']
    priority = row['priority']
    reserved = row['reserved']
    datapage = row['dp']
    pduformat = row['pf']
    pduspecific = row['ps']
    extrabits = row['extraBits']
    stuffingbits = row['stuffingBits']
    timestamp = row['timestamp']

    init_past_values_dict(pgn, sa)

    if(type(row['canSampleValues']) == str):
        spnvalues = json.loads(row['canSampleValues'])
    else:
        spnvalues = json.loads('{}')
        
    frequency = get_frequency(pgn, sa, timestamp)

    #If statement to check values of a row follow the model
    if pgn in settings.model_dict:
        if sa in settings.model_dict[pgn]:
            sa_dict = settings.model_dict[pgn][sa]
            if dlc != sa_dict['DLC']:
                if mode == "test":
                    notifier.invalid_dlc(dlc, row)
            if int(priority) not in sa_dict['Priorities']:
                if mode == "test":
                    notifier.invalid_priority(priority, row)
                elif mode == "train":
                    model_update.update_priority(priority,row)
            if int(reserved) != sa_dict['Reserved']:
                if mode == "test":
                    notifier.invalid_reserved(reserved, row)
            if int(datapage) != sa_dict['DataPage']:
                if mode == "test":
                    notifier.invalid_datapage(datapage, row)
            if int(pduformat) not in sa_dict['PduFormat']:
                if mode == "test":
                    notifier.invalid_pduformat(pduformat, row)
                elif mode == "train":
                    model_update.update_pduformat(pduformat,row)
            if int(pduspecific) not in sa_dict['PduSpecific']:
                if mode == "test":
                    notifier.invalid_pduspecific(pduspecific, row)
                elif mode == "train":
                    model_update.update_pduspecific(pduspecific,row)
            if int(extrabits) not in sa_dict['ExtraBits']:
                if mode == "test":
                    notifier.invalid_extrabits(extrabits, row)
                elif mode == "train":
                    model_update.update_extrabits(extrabits,row)
            if int(stuffingbits) != 0:
                notifier.invalid_stuffingbits(row)
            if frequency != None and frequency > 0: #Needs quite a bit of training as frequency of messages can vary wildly
                if not check_frequency(frequency, sa_dict['Frequencies']):
                    if mode == "test":
                        notifier.invalid_frequency(frequency, row)
                    elif mode == "train":
                        model_update.update_frequency(frequency,row)

                #Checks if the spn values have the correct rate of change
                last_spn_values_dict = settings.past_values_dict.get(pgn, {}).get(sa).get('LastSPNValues')
                for spnvalue in spnvalues:
                    rate_of_change = spnvalue['DecodedSPNValue'] - last_spn_values_dict.get(spnvalue['SPNName'], 0)
                    spnrules_dict = sa_dict['SPNRules'].get(spnvalue['SPNName'])
                    #spnrules_dict != None is when a spn value doesn't exist in the spnrules. 
                    #For example with PGN 60416 which is a multiplex message and therefore can have different SPNvalues per message
                    #This should be removed later on and is a temporary bandaid
                    if spnrules_dict != None:
                        if spnrules_dict.get('MaxValue') is None or spnvalue['DecodedSPNValue'] > spnrules_dict['MaxValue']:
                            model_update.update_max_spn_value(spnvalue['SPNName'], spnvalue['DecodedSPNValue'], row)
                        if spnrules_dict.get('MinValue') is None or spnvalue['DecodedSPNValue'] < spnrules_dict['MinValue']:
                            model_update.update_min_spn_value(spnvalue['SPNName'], spnvalue['DecodedSPNValue'], row)
                        if spnrules_dict.get('States') is None or str(spnvalue['StateValue']) not in spnrules_dict['States']:
                            model_update.update_spn_state(spnvalue['SPNName'], spnvalue['StateValue'], row)
                        if spnrules_dict.get('Unit') is None or spnvalue['SPNUnit'] != spnrules_dict['Unit']:
                            model_update.update_spn_unit(spnvalue['SPNName'], spnvalue['SPNUnit'], row)
                        if spnrules_dict.get('Availability') is None or spnvalue['Available'] != spnrules_dict['Availability']:
                            model_update.update_availability(spnvalue['SPNName'], spnvalue['Available'], row)
                        if rate_of_change > 0:
                            rate_of_change_delta = rate_of_change/frequency
                            if not check_pos_rate_of_change(rate_of_change_delta, spnrules_dict):
                                if mode == "test":
                                    notifier.invalid_pos_rate_of_change_delta(spnvalue['SPNName'], rate_of_change_delta, row)
                                elif mode == "train":
                                    model_update.update_pos_rate_of_change(spnvalue['SPNName'], rate_of_change, row)
                                    model_update.update_pos_rate_of_change_delta(spnvalue['SPNName'], rate_of_change_delta, row)
                        elif rate_of_change < 0:
                            rate_of_change_delta = rate_of_change/frequency
                            if not check_neg_rate_of_change(rate_of_change_delta, spnrules_dict):
                                if mode == "test":
                                    notifier.invalid_neg_rate_of_change_delta(spnvalue['SPNName'], rate_of_change_delta, row)
                                elif mode == "train":
                                    model_update.update_neg_rate_of_change(spnvalue['SPNName'], rate_of_change, row)
                                    model_update.update_neg_rate_of_change_delta(spnvalue['SPNName'], rate_of_change_delta, row)
        else:
            if mode == "test":
                notifier.invalid_sa(sa, row)
            elif mode == "train":
                model_update.update_sa(sa,row)
    else:
        if mode == "test":
            notifier.invalid_pgn(pgn, row)
        elif mode == "train":
            model_update.update_pgn(pgn,row)
    set_last_timestamp(pgn, sa, timestamp)
    set_last_spn_values(pgn, sa, spnvalues)

def process_data(data):
    settings.model_dict = get_model(model_file_path)
    data.apply(test_model, axis=1) #Calculate time for each row