#Python file that stores global variables for use by multiple .py files

def init():
    global model_dict
    global past_values_dict
    past_values_dict = {}
    model_dict = {}
