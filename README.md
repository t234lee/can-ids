# CAN IDS

A rule based CAN Intrusion Detection System that detects the presence of anomalous CAN messages

data/NMEA-Sensor-2023-10-19.zip - Contains 10 minutes of NMEA data collected from a NMEA sensor in the lab
data/TRUCK-1-Door-Idle.zip - Contains 10 minutes of J1939 data collected from the passenger door control unit in the peterbilt truck during an idle state
data/TRUCK-2-OBDII-Idle.zip - Contains 10 minutes of J1939 data collected from the OBDII port in the peterbilt truck during an idle state

To Note: While the frequencies for the NMEA sensor are fine due to highest message frequency being 500 millseconds both the door and OBDII suffer from using NTP servers to sync the palisade boxes. This is due to CAN messages in the peterbilt truck having frequencies in the tens of milliseconds and NTP is only truly accurate within tens of milliseconds. For example looking at the door model you can see three frequencies for PGN 61444, the true frequency seems to line around 20 milliseconds but due to the issues with NTP we see frequencies of 10 milliseconds and 30 milliseconds. This would hopefully be improve by moving over to PTP for time syncing.