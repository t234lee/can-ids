import os
import json
import time

import pandas as pd
from minio import Minio

import settings
import data_processing
import threading

from grafana.data_server import run_app

from dotenv import load_dotenv

load_dotenv()

minio_endpoint = os.getenv('MINIO_ENDPOINT')
minio_access_key = os.getenv('MINIO_ACCESS_KEY')
minio_secret_key = os.getenv('MINIO_SECRET_KEY')
tenant = os.getenv('TENANT_ID')

client = Minio(minio_endpoint, minio_access_key, minio_secret_key)
bucket = "tenant-" + tenant
filepath = os.getenv('SENSOR_FILEPATH')

model_file_path = os.getenv('MODEL_FILEPATH')

#Checks the model file_path exists and creates it if it doesn't
def check_model():
    if not os.path.isfile(model_file_path):
        model = {}
        model_json = json.dumps(model, indent=4)
        with open(model_file_path, "w") as outfile:
            outfile.write(model_json)

def minio_live_data_listener():
    with client.listen_bucket_notification(
        bucket,
        prefix=filepath,
        events=["s3:ObjectCreated:Put"],
    ) as events:
        for event in events:
            data_file_path = event["Records"][0]["s3"]["object"]["key"]
            client.fget_object(bucket,data_file_path,data_file_path)
            data = pd.read_csv(data_file_path, compression='gzip') 
            before_data = time.time_ns()
            data_processing.process_data(data)
            after_data = time.time_ns()
            #Prints out stats on how long data processing took
            row = [before_data, int(data.head(1)['timestamp']), int(data.tail(1)['timestamp']), after_data - before_data]
            print(row)
            if os.path.exists(data_file_path):
                os.remove(data_file_path)
            
def minio_stored_data_listener(): 
    dtfolders = client.list_objects(bucket, prefix=filepath)
    for dtfolder in dtfolders:
        objects = client.list_objects(bucket, prefix=dtfolder.object_name)
        for obj in objects:
            #time.sleep(1) #Simulates times between uploads
            client.fget_object(bucket,obj.object_name,obj.object_name)
            #print(obj.object_name)
            data = pd.read_csv(obj.object_name, compression='gzip')
            data_processing.process_data(data)
            os.remove(obj.object_name)

#Function to check what pgns were decoded and what weren't
def check_decoded_pgns():
    # Opening JSON file
    with open(model_file_path, 'r') as pgn_file:
        # Reading from json file
        valid_pgns = json.load(pgn_file)
    decoded_pgns = []
    undecoded_pgns = []
    for pgn in valid_pgns:
        sa = next(iter(valid_pgns[pgn]))
        spnrules = valid_pgns[pgn][sa]['SPNRules']
        if spnrules == {}:
            undecoded_pgns.append(pgn)
        else:
            decoded_pgns.append(pgn)
    no_undecoded_pgns = len(undecoded_pgns)
    no_decoded_pgns = len(decoded_pgns)
    total_pgns = no_decoded_pgns + no_undecoded_pgns
    print(str(no_decoded_pgns) + " PGNs out of " + str(total_pgns) + " successfully decoded")

if __name__ == "__main__":
    settings.init()
    check_model()
    check_decoded_pgns()

    ##Unstable runs grafana plus CAN IDS
        # Create a thread for minio_live_data_listener
        #minio_thread = threading.Thread(target=minio_live_data_listener, daemon=True)
        #minio_thread.start()

        # Run Flask in the main thread
        #run_app()
    ##Unstable runs grafana plus decoder

    #minio_live_data_listener()
    minio_stored_data_listener() #This is for using stored minio data instead of live
