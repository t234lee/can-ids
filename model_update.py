#Python file to update the model during the training phase
import os
import json

import settings

from dotenv import load_dotenv

load_dotenv()

model_file_path = os.getenv('MODEL_FILEPATH')

def set_model(model_file_path):
    model_json = json.dumps(settings.model_dict, indent=4)
    with open(model_file_path, "w") as outfile:
        outfile.write(model_json)

def set_sa_dict(row):
    sa_dict = {'DLC':row['dlc'],"Priorities":[row['priority']],"Reserved":row['reserved'],"DataPage":row['dp'],"PduFormat":[row['pf']],"PduSpecific":[row['ps']],"ExtraBits":[row['extraBits']],"Frequencies":[]}
    if(type(row['canSampleValues']) == str):
        spnvalues = json.loads(row['canSampleValues'])
    else:
        spnvalues = json.loads('{}')
    spn_rules_dict = {}
    for spn in spnvalues:
        rate_of_change_dict = {}
        rate_of_change_dict["MaxValue"] = None
        rate_of_change_dict["MinValue"] = None 
        rate_of_change_dict["PositiveRateOfChange"] = 0
        rate_of_change_dict["NegativeRateOfChange"] = 0
        rate_of_change_dict["PositiveRateOfChangeDelta"] = 0
        rate_of_change_dict["NegativeRateOfChangeDelta"] = 0
        rate_of_change_dict["States"] = []
        rate_of_change_dict["Unit"] = None
        rate_of_change_dict["Availability"] = None
        spn_rules_dict[spn['SPNName']] = rate_of_change_dict
    sa_dict['SPNRules'] = spn_rules_dict
    return sa_dict

def update_pgn(pgn, row):
    sa_dict = set_sa_dict(row)
    pgn_dict = {}
    pgn_dict[str(row['sa'])] = sa_dict
    settings.model_dict[pgn] = pgn_dict
    set_model(model_file_path)

def update_sa(sa, row):
    sa_dict = set_sa_dict(row)
    settings.model_dict[str(row['pgn'])][sa] = sa_dict
    set_model(model_file_path)

def update_priority(priority, row):
    settings.model_dict[str(row['pgn'])][str(row['sa'])]['Priorities'].append(priority)
    set_model(model_file_path)

def update_pduformat(pduformat, row):
    settings.model_dict[str(row['pgn'])][str(row['sa'])]['PduFormat'].append(pduformat)
    set_model(model_file_path)

def update_pduspecific(pduspecific, row):
    settings.model_dict[str(row['pgn'])][str(row['sa'])]['PduSpecific'].append(pduspecific)
    set_model(model_file_path)

def update_extrabits(extrabits, row):
    settings.model_dict[str(row['pgn'])][str(row['sa'])]['ExtraBits'].append(extrabits)
    set_model(model_file_path)

def update_frequency(frequency, row):
    settings.model_dict[str(row['pgn'])][str(row['sa'])]['Frequencies'].append(frequency)
    set_model(model_file_path)

def update_pos_rate_of_change(spn_name, rate_of_change, row):
    settings.model_dict[str(row['pgn'])][str(row['sa'])]['SPNRules'][spn_name]['PositiveRateOfChange'] = rate_of_change
    set_model(model_file_path)

def update_neg_rate_of_change(spn_name, rate_of_change, row):
    settings.model_dict[str(row['pgn'])][str(row['sa'])]['SPNRules'][spn_name]['NegativeRateOfChange'] = rate_of_change
    set_model(model_file_path)
    
def update_pos_rate_of_change_delta(spn_name, rate_of_change_delta, row):
    settings.model_dict[str(row['pgn'])][str(row['sa'])]['SPNRules'][spn_name]['PositiveRateOfChangeDelta'] = rate_of_change_delta
    set_model(model_file_path)

def update_neg_rate_of_change_delta(spn_name, rate_of_change_delta, row):
    settings.model_dict[str(row['pgn'])][str(row['sa'])]['SPNRules'][spn_name]['NegativeRateOfChangeDelta'] = rate_of_change_delta
    set_model(model_file_path)

def update_availability(spn_name, availablity, row):
    settings.model_dict[str(row['pgn'])][str(row['sa'])]['SPNRules'][spn_name]['Availability'] = availablity
    set_model(model_file_path)

def update_max_spn_value(spn_name, maxValue, row):
    settings.model_dict[str(row['pgn'])][str(row['sa'])]['SPNRules'][spn_name]['MaxValue'] = maxValue
    set_model(model_file_path)

def update_min_spn_value(spn_name, minValue, row):
    settings.model_dict[str(row['pgn'])][str(row['sa'])]['SPNRules'][spn_name]['MinValue'] = minValue
    set_model(model_file_path)

def update_spn_unit(spn_name, unit, row):
    settings.model_dict[str(row['pgn'])][str(row['sa'])]['SPNRules'][spn_name]['Unit'] = unit
    set_model(model_file_path)

def update_spn_state(spn_name, state, row):
    settings.model_dict[str(row['pgn'])][str(row['sa'])]['SPNRules'][spn_name]['States'].append(str(state))
    set_model(model_file_path)